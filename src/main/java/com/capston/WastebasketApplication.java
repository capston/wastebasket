package com.capston;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WastebasketApplication {

	public static void main(String[] args) {
		SpringApplication.run(WastebasketApplication.class, args);
	}
}
